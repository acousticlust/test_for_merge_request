import platform
import sys

info = 'OS info is \n{}\n\nPython version is {} {}'.format(
    platform.uname(),
    sys.version,
    platform.architecture(),
)
print(info)

with open('os_info.txt', 'w', encoding='utf8') as file:
    file.write(info)


print('Ответ на задачу 2')

def count_of_digits(n):
    return len(str(n))

def sum_of_digits(n):
    return sum(int(i) for i in str(n))

num = int(input("Введите число: "))

sums = sum_of_digits(num)
counts = count_of_digits(num)
print("\nСумма цифр:", sums)
print("\nКол-во цифр в числе:", counts)

print("\nРазность суммы и кол-ва цифр:", sums - counts)

print('Ответ на задачу 3')

n = int(input("Введите число больше чем 1: "))
i = 2
while n % i:
    i += 1
print(i)